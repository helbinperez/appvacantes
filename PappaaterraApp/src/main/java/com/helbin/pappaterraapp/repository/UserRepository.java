package com.helbin.pappaterraapp.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.helbin.pappaterraapp.model.User;

public interface UserRepository extends JpaRepository<User, Long> {

}
