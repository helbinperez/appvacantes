package com.helbin.pappaterraapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.helbin.pappaterraapp.model.Vacantes;

import java.util.List;

@Repository
public interface VacantesRepository extends JpaRepository<Vacantes, Long>{
    List<Vacantes> findByStatus(int status);
}
