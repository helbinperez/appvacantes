package com.helbin.pappaterraapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.helbin.pappaterraapp.model.Categoria;

@Repository
public interface CategoriasRepository extends JpaRepository<Categoria, Long>{

	public List<Categoria> findByNombre(String nombre);
}
