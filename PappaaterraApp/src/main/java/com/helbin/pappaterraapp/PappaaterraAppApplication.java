package com.helbin.pappaterraapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PappaaterraAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(PappaaterraAppApplication.class, args);
	}

}
