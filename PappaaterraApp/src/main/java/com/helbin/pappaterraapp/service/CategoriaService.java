package com.helbin.pappaterraapp.service;

import java.util.List;
import java.util.Optional;

import com.helbin.pappaterraapp.model.Categoria;

public interface CategoriaService {
	
	public List<Categoria> findAll();
	
	public Categoria findById1(Long id);
	
	public Optional<Categoria> findById(Long id);

	public List<Categoria> findByNombre(String nombre);
	
	public Categoria save(Categoria categoria);
	
	
}
