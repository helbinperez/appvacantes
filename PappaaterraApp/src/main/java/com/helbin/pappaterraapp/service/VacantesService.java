package com.helbin.pappaterraapp.service;

import java.util.List;

import com.helbin.pappaterraapp.model.Vacantes;

public interface VacantesService {
	
	List<Vacantes> findAll();
	Vacantes findById(Long id);
	Vacantes save(Vacantes vacante);
	List<Vacantes> findByStatus(int status);
	void delete(Vacantes id);
}
