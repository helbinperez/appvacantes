package com.helbin.pappaterraapp.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.helbin.pappaterraapp.model.Categoria;
import com.helbin.pappaterraapp.repository.CategoriasRepository;

@Service
public class CategoriaServiceImpl implements CategoriaService{

	@Autowired
	private CategoriasRepository categeroiaRepository;
	
	@Override
	public List<Categoria> findAll() {
		return categeroiaRepository.findAll();
	}

	@Override
	public List<Categoria> findByNombre(String nombre) {
		List<Categoria> categoriaList = new ArrayList<>();
	
		for(Categoria cat : findAll()) {
			if(cat.getNombre().equals(nombre)) {
				categoriaList = categeroiaRepository.findByNombre(nombre);
			}
		}
		
		return categoriaList;
	}

	@Override
	public Optional<Categoria> findById(Long id) {
		Optional<Categoria> optional = categeroiaRepository.findById(id);
		if(optional.isPresent()) {
			return Optional.of(optional.get());
		}
		return null;
	}

	@Override
	public Categoria save(Categoria categoria) {
		return categeroiaRepository.save(categoria);
	}

	@Override
	public Categoria findById1(Long id) {
		// TODO Auto-generated method stub
		//TEMPORAL
		return categeroiaRepository.getById(id);
	}
	
}
