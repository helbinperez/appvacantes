package com.helbin.pappaterraapp.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.helbin.pappaterraapp.model.Vacantes;
import com.helbin.pappaterraapp.repository.VacantesRepository;
import org.springframework.web.bind.annotation.GetMapping;

@Service
public class VacantesServiceImp implements VacantesService {

	@Autowired
	public VacantesRepository vacantesRepository;
	
	@Override
	public List<Vacantes> findAll() {
		return  vacantesRepository.findAll();
	}

	@Override
	public Vacantes findById(Long id) {
		Optional<Vacantes> optional = vacantesRepository.findById(id);
		if(optional.isPresent()){
			return optional.get();
		}
		return null;
	}

	@Override
	public Vacantes save(Vacantes vacante) {
		return vacantesRepository.save(vacante);
	}

	@Override
	public List<Vacantes> findByStatus(int status) {
		List<Vacantes> vacantesList = new ArrayList<>();

		for(Vacantes vacante: findAll()){
			if(vacante.getStatus() == 1){
				vacantesList = vacantesRepository.findByStatus(status);
			}
		}
		return vacantesList;
	}

	@Override
	public void delete(Vacantes id) {
		vacantesRepository.delete(id);
	}

}
