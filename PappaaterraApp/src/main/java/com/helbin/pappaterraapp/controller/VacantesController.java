package com.helbin.pappaterraapp.controller;

import com.helbin.pappaterraapp.model.Vacantes;
import com.helbin.pappaterraapp.service.VacantesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/vacantes")
public class VacantesController {

    @Autowired
    private VacantesService vacantesService;

    @GetMapping("/listar")
    public List<Vacantes> findAll(){
        return vacantesService.findAll();
    }

    @GetMapping("/buscar/{id}")
    public Vacantes findById(@PathVariable("id") Long id){
        return vacantesService.findById(id);
    }

    @PostMapping("/save")
    public Vacantes save(@RequestBody Vacantes vacantes){
        return vacantesService.save(vacantes);
    }

    @PutMapping("/update")
    public Vacantes update(Vacantes vacante, Long id){
        Vacantes vacantesActual =  vacantesService.findById(id);

        vacantesActual.setNombre(vacante.getNombre());
        vacantesActual.setDescripcion(vacante.getDescripcion());
        vacantesActual.setFecha(vacante.getFecha());
        vacantesActual.setSalari(vacante.getSalari());
        vacantesActual.setStatus(vacante.getStatus());
        vacantesActual.setDestacado(vacante.getDestacado());
        vacantesActual.setImage(vacante.getImage());
        vacantesActual.setDetalles(vacante.getDetalles());
        vacantesActual.setCategoria(vacante.getCategoria());

        return vacantesActual;

    }

    @DeleteMapping("borrar/{id}")
    public void delete(Vacantes id){
        vacantesService.delete(id);
    }
}
