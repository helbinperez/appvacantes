package com.helbin.pappaterraapp.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.helbin.pappaterraapp.model.Categoria;
import com.helbin.pappaterraapp.service.CategoriaService;

@RestController
@RequestMapping("/api")
public class CategoriasController {

	@Autowired
	private CategoriaService categoriaService;
	
	@GetMapping("/categorias")
	public List<Categoria> findAll(){
		return categoriaService.findAll();
	}
	
	@GetMapping("/categorias/{id}")
	public Optional<Categoria> findById(@PathVariable(name="id") Long id) {
		return categoriaService.findById(id);
	}
	
	@GetMapping("/categorias/buscar/{nombre}")
	public List<Categoria> findByNombre(@PathVariable(name="nombre") String nombre){
		return categoriaService.findByNombre(nombre);
	}
	
	@PostMapping("/categorias/save")
	@ResponseStatus(HttpStatus.CREATED)
	public Categoria save(@RequestBody Categoria categoria) {
		return categoriaService.save(categoria);
	}
	
	@PutMapping("/categorias/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public Categoria update(Categoria categoria, @PathVariable Long id) {
		Categoria categoriaActual = categoriaService.findById1(id); 
		categoriaActual.setNombre(categoria.getNombre());
		categoriaActual.setDescripcion(categoria.getDescripcion());
		
		return categoriaService.save(categoriaActual);
	}
}
